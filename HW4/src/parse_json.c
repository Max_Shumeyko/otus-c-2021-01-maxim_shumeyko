#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "parson.h"

struct parse_json = {
    pj__parse_json,
    pj__get_num,
    pj__get_string_array,
    pj__free,
    pj__free_string_array,
    pj__get_array_element
};


static bool check_is_NULL(const char* message, void* pointer)
{
    if (pointer == NULL)
    {
        perror(message);
        return true;
    }

    return false;
}

JSON_Value* 
pj__parse_json(const char* string)
{
    JSON_Value* json_value = json_parse_string(string);
    
    if (json_value_get_type(json_value) == -1)
    {
        perror("parse_json: input string is not a valid json");
        return NULL;
    }

    return json_value;
}

void pj__free(JSON_Value* json_value)
{
    json_value_free(json_value);
}

JSON_Object* 
pj__get_array_element(JSON_Value* json_value, unsigned int ind)
{   
    JSON_Object* json_object = NULL;

    if (json_value_get_type(json_value) != JSONArray)
    {
        perror("pj__get_array_element: input string is not an array json");
        return NULL;
    }

    JSON_Array* array = json_value_get_array(json_value);
    if (ind > json_array_get_count(array))
    {
        perror("pj__get_array_element: index is out of array bounds");
        return NULL;
    }
    
    json_object = json_array_get_object(array, 0);

    return json_object;
}

char* 
pj__get_num(JSON_Object* json_object, 
            const char* key)
{
    float num = json_object_get_number(json_object, key);
    char* result = (char*) calloc(20, 1); 
    sprintf(result, "%d", (int)num);

    return result;
}

char** 
pj__get_string_array(JSON_Value*  json_value, 
                     const char*  path,
                     const char*  key,
                     int*         output_size)
{
    JSON_Object* json_object = json_value_get_object(json_value);
    JSON_Array* inner_objects = json_object_dotget_array(json_object, path);

    if (!inner_objects)
    {
        perror("pj__get_array: no inner objects found");
        return NULL;
    }
    
    int array_size = json_array_get_count(inner_objects); 
    if (array_size == 0)
    {
        perror("No elements were found.\n");
        return NULL;
    }
    *output_size = array_size;

    // allocate string array
    char** result = (char**) malloc(array_size * sizeof(char*));
    if (check_is_NULL("Memory error", result)) {
        return NULL;
    }

    int slen;
    int i;

    for (i = 0; i < array_size; i++) 
    {
        JSON_Object* obj = json_array_get_object(inner_objects, i);
        char* cur_str;

        // check type of key value and read to string
        if (json_object_has_value_of_type(obj, key, JSONNumber))
        {
            float num = json_object_get_number(obj, key);
            cur_str = (char*) calloc(20, 1); 
            sprintf(cur_str, "%.3f", num);
        } 
        else if (json_object_has_value_of_type(obj, key, JSONString))
        {
            const char* cur_str_sub = json_object_get_string(obj, key);
            cur_str = strdup(cur_str_sub);
        } 
        else
        { 
            printf("pj__get_string_array: wrong value type. Key:{%s}\n", key);
            continue;
        }

        // write value to the string array
        slen = strlen(cur_str);
        result[i] = (char*) malloc(slen + 1);
        if (check_is_NULL("Memory error", result[i])) {
            return NULL;
        }

        strncpy(result[i], cur_str, slen);
        result[i][slen] = '\0';
        free(cur_str);

    }

    return result;
}

void 
pj__free_string_array(char** string_array,
                      int size)
{
    for (int i = 0; i < size; i++)
        free(string_array[i]);    
    free(string_array);
}




