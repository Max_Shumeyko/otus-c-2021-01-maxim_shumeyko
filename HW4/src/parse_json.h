#ifndef __PJ_H_
#define __PH_H_

JSON_Value*      
pj__parse_json(const char* string);

char* 
pj__get_num(JSON_Object*    json_object, 
            const char*     key);

char** 
pj__get_string_array(JSON_Value*    json_value, 
                     const char*    path,
                     const char*    key,
                     int*           output_size);

void 
pj__free(JSON_Value* json_object);

void 
pj__free_string_array(char**    string_array,
                      int       size);

JSON_Object* 
pj__get_array_element(JSON_Value* json_value, unsigned int ind);

// define parse_json interface for parson
struct pj
{
    JSON_Value*     (*parse)(const char*);
    char*           (*get_num)(JSON_Object*,
                                  const char*);
    char**          (*get_string_array)(JSON_Value*,
                                        const char*,
                                        const char*,
                                        int*);
    void            (*free)(JSON_Value*);
    void            (*free_string_array)(char**,
                                         int);

    JSON_Object*    (*get_array_element)(JSON_Value*,
                                         unsigned int);

};

extern struct pj
parse_json;

#endif // __PJ_H_
