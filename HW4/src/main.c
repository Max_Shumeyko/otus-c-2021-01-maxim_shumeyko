#include <stdio.h>

#include "get_weather.h"

int main(int argc, char** argv)
{

    if (argc != 2) 
    {
        printf("USAGE: %s <location> (moscow/london/..)\n", argv[0]);
        return 1;
    }

    int err_check = get_weather(argv[1]);

    if (!err_check)
    {
        printf("Finish work.\n");
    }

    return 0;
}

