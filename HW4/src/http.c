#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

char* do_request(const char* request_URL)
{
    // run request in subprocess
    pid_t pid = fork();
    int status;
    // subprocess
    if (pid == 0) 
    {
        execl("./my_curl_request/do_request", "./my_curl_request/do_request", 
              request_URL, NULL);
    }

    waitpid(pid, &status, 0);
    if (WEXITSTATUS(status))
    {
        printf("Subprocess crushed.\n");
        return NULL;
    }

    FILE* f = fopen("report.txt", "r");

    if (!f)
    {
        printf("do_request: IO error.\n");
        fclose(f);
        return NULL;
    }

    fseek(f, 0L, SEEK_END);
    long length = ftell(f);
    fseek(f, 0L, SEEK_SET);
    char* buffer = (char*) malloc(length + 1);

    if (!buffer)
    {
        printf("Memory allocation error.\n");
        return NULL;
    }
    fread(buffer, 1, length, f);
    buffer[length] = '\0';

    if (ferror(f))
    {
        printf("do_request: file error. Return buffer and flush.\n");
        fflush(f);
        return buffer;
    }
    fclose(f); 
    system("rm report.txt");

    return buffer;
}
