#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parson.h"
#include "http.h"
#include "parse_json.h"

#define URL_SIZE 800

int get_weather(const char* location)
{
    const char* request_pattern = "https://www.metaweather.com/api/location/";

    // prepare location request
    char request_URL[URL_SIZE];
    strncpy(request_URL, request_pattern, URL_SIZE); 
    strcat(request_URL, "search/?query=");
    strcat(request_URL, location);

    // upload location data
    char* location_json = do_request(request_URL);
    if (!location_json || (strcmp(location_json, "[]") == 0))
    {
        printf("Error: wrong input location!\n");
        return 1;
    }

    // extract woeid index for the weather request
    JSON_Value* location_jvalue = parse_json.parse(location_json);
    if (!location_jvalue)
        return 1;

    JSON_Object* location_jobj = 
        parse_json.get_array_element(location_jvalue, 0);
    char* woeid = parse_json.get_num(location_jobj, "woeid");
    if (!woeid)
        return 1;
    
    // prepare weather request
    request_URL[0] = '\0'; // reinit
    strncpy(request_URL, request_pattern, URL_SIZE);
    strcat(request_URL, woeid);
    
    printf("Getting weather data...\n");
    // upload weather data
    char* weather_json = do_request(request_URL);
    if (!weather_json)
        return 1;

    // extract weather data
    JSON_Value* weather_jvalue = parse_json.parse(weather_json);
    if (!weather_jvalue)
        return 1;

    int ds_size;
    char** datestamps;
    datestamps = parse_json.get_string_array(
        weather_jvalue,
        "consolidated_weather",
        "created",
        &ds_size
    );
    if (!datestamps)
        return 1;

    int ws_size;
    char** weather_states;
    weather_states = parse_json.get_string_array(
        weather_jvalue,
        "consolidated_weather",
        "weather_state_name",
        &ws_size
    );
    if (!weather_states)
        return 1;

    int wd_size;
    char** wind_directions;
    wind_directions = parse_json.get_string_array(
        weather_jvalue,
        "consolidated_weather",
        "wind_direction_compass",
        &wd_size
    );
    if (!wind_directions)
        return 1;

    int wspd_size;
    char** wind_speeds; // not sure if it's even legal
    wind_speeds = parse_json.get_string_array(
        weather_jvalue,
        "consolidated_weather",
        "wind_speed",
        &wspd_size
    );
    if (!wind_speeds)
        return 1;

    int mintmp_size;
    char** min_temperatures = parse_json.get_string_array( weather_jvalue,
        "consolidated_weather",
        "min_temp",
        &mintmp_size
    );
    if (!min_temperatures)
        return 1;

    int maxtmp_size;
    char** max_temperatures = parse_json.get_string_array(
        weather_jvalue,
        "consolidated_weather",
        "the_temp",
        &maxtmp_size
    );
    if (!max_temperatures)
        return 1;

    // check if data arrays sizes are equal
    if (!((ds_size == ws_size) && (ws_size == wd_size) 
           && (wd_size == wspd_size) && (wspd_size = mintmp_size)
           && (mintmp_size == maxtmp_size)
         )
       )
    { 
        printf("Array sizes aren't equal.\n");
        return 2;
    }

    // print the report
    int array_size = ds_size;


    printf("\n--------WEATHER REPORT-----------\n\n");
    for (int i = 0; i < array_size; i++)
    {
        printf("**************************\n");
        printf("Created: %s\n", datestamps[i]);
        printf("Weather state: %s\n", weather_states[i]);
        printf("Wind direction: %s\n", wind_directions[i]);
        printf("Wind speed: %s\n", wind_speeds[i]);
        printf("Min temperature: %s\n", min_temperatures[i]);
        printf("Max temperature: %s\n", max_temperatures[i]);
    }
    printf("------------------END---------------\n");

    // free allocated variables
    free(woeid);
    free(location_json);
    free(weather_json);

    parse_json.free(location_jvalue);
    parse_json.free(weather_jvalue);
    parse_json.free_string_array(datestamps, array_size);
    parse_json.free_string_array(weather_states, array_size);
    parse_json.free_string_array(wind_directions, array_size);
    parse_json.free_string_array(wind_speeds, array_size);
    parse_json.free_string_array(min_temperatures, array_size);
    parse_json.free_string_array(max_temperatures, array_size);

    return 0;
}
