#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <curl/curl.h>

typedef struct {
    char* memory;
    size_t size;
} buffer_t;

static size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;
    buffer_t* buff = (buffer_t*) userp;

    char* ptr = realloc(buff->memory, buff->size + realsize + 1);
    if (ptr == NULL) {
        /* out of memory! */
        perror("realloc crushed\n");
        return 0;
    }

    buff->memory = ptr;
    memcpy(&(buff->memory[buff->size]), contents, realsize);
    buff->size += realsize;
    buff->memory[buff->size] = 0;

    return realsize;
}

char* do_request(const char *URL)
{
    CURL* curl = curl_easy_init();

    printf("Request {%s} in process... ", URL);

    if (!curl) {
        perror("curl_easy_init crushed");
        return NULL;
    }

    buffer_t buffer;
    buffer.memory = malloc(1); 
    buffer.size = 0;
    
    curl_easy_setopt(curl, CURLOPT_URL, URL);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*) &buffer);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);

    if (curl_easy_perform(curl) != CURLE_OK)
    {
        perror("curl_easy_perform: failed\n");
    }

    curl_easy_cleanup(curl);

    if (!buffer.size)
    {
        printf("Error: upload no data.\n");
        return NULL;
    }
    printf("Done.\n");

    return buffer.memory;
}
