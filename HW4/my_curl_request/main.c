#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#include "http.h"
#include <curl/curl.h>

int main(int argc, char** argv)
{

    if (argc != 2)
    {
        printf("Usage: %s <url>\n", argv[0]);
        return 1;
    }

    curl_global_init(CURL_GLOBAL_ALL);
    char* result = do_request(argv[1]);
    curl_global_cleanup();

    if (!result)
    {
        printf("Empty link or libcurl can't handle it. Trying wget...\n");

        pid_t pid = fork();
        int status;
        if (pid == 0) 
        {
            char* args[] = {"/usr/bin/wget", "-O", 
                            "report.txt", argv[1], NULL};
            execvp("/usr/bin/wget", args);
        }
        waitpid(pid, &status, 0);
        return 0;
    }
        
  
    FILE* f = fopen("report.txt", "w");
    fwrite(result, strlen(result), 1, f);
    fclose(f);

    return 0;
}

 

