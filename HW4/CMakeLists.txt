cmake_minimum_required(VERSION 3.5)
project(check_weather C)

set(CMAKE_C_STANDARD 11)

add_subdirectory("contrib/parson")
include_directories("contrib/parson")

add_subdirectory("my_curl_request")
add_compile_options(-Wall -Wextra -Wpedantic)

set(SOURCES
    "src/main.c"
    "src/get_weather.c"
    "src/http.c"
    "src/parse_json.c"
)

set(HEADERS
    "src/get_weather.h"
    "src/http.h"
    "src/parse_json.h"
)

add_executable(check_weather ${SOURCES} ${HEADERS})
target_link_libraries(check_weather parson)
