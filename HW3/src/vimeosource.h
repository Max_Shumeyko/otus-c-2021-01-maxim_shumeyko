/* GStreamer
 * Copyright (C) 2021 Andrew Kravchuk <awkravchuk@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __VIMEOSOURCE_H_
#define __VIMEOSOURCE_H_

#include <gst/base/gstpushsrc.h>

#include <curl/curl.h>


G_BEGIN_DECLS

#define _TYPE_VIMEOSOURCE (_vimeosource_get_type())
#define _VIMEOSOURCE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), _TYPE_VIMEOSOURCE, VimeoSource))
#define _VIMEOSOURCE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), _TYPE_VIMEOSOURCE, VimeoSourceClass))
#define _IS_VIMEOSOURCE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), _TYPE_VIMEOSOURCE))
#define _IS_VIMEOSOURCE_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((klass), _TYPE_VIMEOSOURCE))

typedef struct _VimeoSource VimeoSource;
typedef struct _VimeoSourceClass VimeoSourceClass;

struct _VimeoSource
{
    GstPushSrc base_vimeosource;
    gchar* file_location;

    CURLM* curlm;
    CURL* curl;
    GstBuffer* current_buffer;
};

struct _VimeoSourceClass
{
    GstPushSrcClass base_vimeosource_class;
};

GType _vimeosource_get_type(void);

G_END_DECLS

#endif
