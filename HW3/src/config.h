
#pragma once

#include <glib.h>

gchar* get_config_url(const gchar* video_url);
gchar* get_file_url(const gchar* video_url);
