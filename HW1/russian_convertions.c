#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

#include "russian_convertions.h"

typedef unsigned short int (*f_to_unicode)(char);

/* Specify available converation functions here.
 * Don't forget to write them also into .h file
 */
char *encodings[] = {"koi8", "iso_8859_5", "cp1251"};
f_to_unicode convert_functions[] = {koi8_to_unicode, iso8859_5_to_unicode, cp1251_to_unicode};

f_to_unicode get_unicode_convert_function(char *encoding)
{
    int N = sizeof(encodings) / sizeof(encodings[0]);
    for (int i = 0; i < N; i++) 
       if (strcmp(encoding, encodings[i]) == 0)
           return convert_functions[i];

    assert(!"wrong input encoding value");
    printf("wrong input encoding value");
    return NULL;
}

/* Converts iso_8859_5 number to unicode 
 * see https://kvodo.ru/tablitsa-iso-8859-5.html for details
 */
unsigned short int iso8859_5_to_unicode(char ch)
{
    unsigned char c = ch;
    assert(c >= 176 && c <= 239 && "wrong input");

    if (!(c >= 176 && c <= 239))
    {
        printf("wrong input");
        exit(0);
    }

    return c + 864;
}

/* Converts koi8_r to unicode 
 * see https://kvodo.ru/tablitsa-koi8.html for details
 */
unsigned short int koi8_to_unicode(char ch)
{
    unsigned char c = ch;

    switch (c) {

    case 192:
        return 0x044E;
    
    case 193:
        return 0x0430;

    case 194:
        return 0x0431;

    case 195:
        return 0x0446;

    case 196:
        return 0x0434;

    case 197:
        return 0x0435;

    case 198:
        return 0x0444;

    case 199:
        return 0x0433;

    case 200:
        return 0x0445;

    case 201:
        return 0x0438;

    case 202:
        return 0x0439;

    case 203:
        return 0x043A;

    case 204:
        return 0x043B;

    case 205:
        return 0x043C;

    case 206:
        return 0x043D;

    case 207:
        return 0x043E;

    case 208:
        return 0x043F;

    case 209:
        return 0x044F;

    case 210:
        return 0x0440;

    case 211:
        return 0x0441;

    case 212:
        return 0x0442;

    case 213:
        return 0x0443;

    case 214:
        return 0x0436;

    case 215:
        return 0x0432;

    case 216:
        return 0x044C;

    case 217:
        return 0x044B;

    case 218:
        return 0x0437;

    case 219:
        return 0x0448;

    case 220:
        return 0x044D;

    case 221:
        return 0x0449;

    case 222:
        return 0x0447;

    case 223:
        return 0x044A;

    case 224:
        return 0x042E;

    case 225:
        return 0x0410;

    case 226:
        return 0x0411;

    case 227:
        return 0x0426;

    case 228:
        return 0x0414;

    case 229:
        return 0x0415;

    case 230:
        return 0x0424;

    case 231:
        return 0x0413;

    case 232:
        return 0x0425;

    case 233:
        return 0x0418;

    case 234:
        return 0x0419;

    case 235:
        return 0x041A;

    case 236:
        return 0x041B;

    case 237:
        return 0x041C;

    case 238:
        return 0x041D;

    case 239:
        return 0x041E;

    case 240:
        return 0x041F;

    case 241:
        return 0x042F;

    case 242:
        return 0x0420;

    case 243:
        return 0x0421;

    case 244:
        return 0x0422;

    case 245:
        return 0x0423;

    case 246:
        return 0x0416;

    case 247:
        return 0x0412;

    case 248:
        return 0x042C;

    case 249:
        return 0x042B;

    case 250:
        return 0x0417;

    case 251:
        return 0x0428;

    case 252:
        return 0x042D;

    case 253:
        return 0x0429;

    case 254:
        return 0x0427;

    case 255:
        return 0x042A;

    default:
        assert(!"wrong input");

        printf("wrong input");
        exit(0);

    }

    return 1;
}

/* Converts cp1251 number to unicode
 * see https://kvodo.ru/tablitsa-windows-1251.html for details
 */
unsigned short int cp1251_to_unicode(char ch)
{
    unsigned char c = ch;
    assert(c >= 192 && "wrong input");

    if (!(c >= 192)) 
    {
        printf("wrong input");
        exit(0);
    }

    return c + 848;
}

// Converts 2 bytes num into utf-8 2 bytes char array and writes it to output.
void unicode_to_utf8(unsigned short int num, unsigned char* output)
{
    int i = 0, base = 1;

    output[0] = 0;
    output[1] = 0;

    while (num) {

        if (i < 6) {
            output[1] += (num & 1) * base; 
        } else if (i < 11) {
            output[0] += (num & 1) * base;
        } else {
            assert(!"wrong input");

            printf("wrong input");
            exit(0);
        }

        i++;
        num >>= 1;
        base *= 2;
        if (i == 6) base = 1;

    }

    output[0] += 192; 
    output[1] += 128;
}

void convert_to_utf8(char c, char *encoding, unsigned char* output)
{
    f_to_unicode to_unicode = get_unicode_convert_function(encoding);

    if (to_unicode == NULL)
    {
        printf("Wrong convertion function");
        exit(0);
    }

    unicode_to_utf8(to_unicode(c), output);
}

