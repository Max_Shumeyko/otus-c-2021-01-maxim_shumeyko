#ifndef R_CONVERTIONS
#define R_CONVERTIONS

// Convertation functions: char -> unicode num
unsigned short int iso8859_5_to_unicode(char);
unsigned short int koi8_to_unicode(char);
unsigned short int cp1251_to_unicode(char);

// Converts 2 bytes num into utf-8 2 bytes char array and writes it to output.
void unicode_to_utf8(unsigned short int, unsigned char* );

// Converts char to utf-8 2-bytes char array and writes it to output.
void convert_to_utf8(char, char*, unsigned char* );

#endif
