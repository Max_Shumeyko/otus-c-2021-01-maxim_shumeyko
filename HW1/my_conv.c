#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

#include "russian_convertions.h"

#define N 2

/*
 *  Converts the given file from one of default russian encodings
 *  (koi8_r, iso_8859_5, cp1251) to utf-8. f
 *  Works only for the default first 127 ASCII symbols and 
 *  russian text!
 *
 *  EXAMPLE:
 *  $./my_conv -i input.txt -o output.txt -e cp1251
 */
int main(int argc, char *argv[])
{
    // define section
    char *fname_in, *fname_out, *encoding;
    char c;
    unsigned char c_out[N] = {0, 0};
    FILE *finput, *foutput;

    // process input parameters
    assert(argc == 7 && "There must be 7 arguments!"
           "aka: $./my_conv -i input.txt -o output.txt -e cp1251"
           "HINT! encodings: koi8, iso_8859_5, cp1251");

    if (argc != 7)
    {
        printf("There must be 7 arguments!"
           "aka: $./my_conv -i input.txt -o output.txt -e cp1251"
           "HINT! encodings: koi8, iso_8859_5, cp1251");

        return 1;
    }



    while ((c = getopt(argc, argv, "i:o:e:")) != EOF)
        switch(c) {

        case 'i':
            fname_in = optarg;
            break;

        case 'o':
            fname_out = optarg;
            break;

        case 'e':
            encoding = optarg;
            break;

        default:
            fprintf(stderr, "Unknown parameter: '%s'\n", optarg);
            return 1;

        }

    // open files
    finput = fopen(fname_in, "r");
    foutput = fopen(fname_out, "w");

    assert(finput);
    assert(foutput);

    // read & convertation cycle
    while (fread(&c, 1, 1, finput) == 1) {

        if ((unsigned char) c <= 127) {
            fputc(c, foutput);
            continue;
        }

        convert_to_utf8(c, encoding, c_out);
        fwrite(c_out, sizeof(char), N, foutput);    

    }

    // close
    fclose(finput);
    fclose(foutput);

    return 0;
}
