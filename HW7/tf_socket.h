#ifndef __TF_SOCKET__
#define __TF_SOCKET__

int
start_socket(int* sock_id, char* server_name);

#endif
