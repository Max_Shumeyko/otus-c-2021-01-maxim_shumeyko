#ifndef __READ_CONFIGURE__
#define __READ_CONFIGURE__

#include "data_types.h"

void read_configure(struct parameters_* parameters);

#endif
