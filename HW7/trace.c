#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <libgen.h>
#include <errno.h>

#include <sys/stat.h>
#include <sys/poll.h>
#include <sys/inotify.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>

#define MAX_EVENTS 1024
#define BUF_SIZE 1024
#define LEN_NAME 16
#define EVENT_SIZE (sizeof (struct inotify_event))
#define BUF_LEN (MAX_EVENTS * (EVENT_SIZE + LEN_NAME))
static char data[BUF_SIZE] = "NO DATA\n";

int 
get_filesize(char* filename)
{
    struct stat buf;
    int st_ind = stat(filename, &buf);
    if (st_ind == -1)
    {
        perror("stat");
        exit(1);
    }

    return buf.st_size;
}

int
process_request(int sock_fd)
{
    char buf[BUF_SIZE];

    int msgsock = accept(sock_fd, 0, 0);
    if (msgsock == -1)
    {
        perror("accept");
        return -1;
    }

	// process
    bzero(buf, BUF_SIZE);
    int rval = recv(msgsock, buf, BUF_SIZE, 0);
    if (rval < 0)
        perror("recv");
    if (rval == 0)
        printf("ending connection");
    if (rval > 0)
        if (strstr(buf, "GET") != NULL)
            send(msgsock, data, strlen(data), 0);

    return 0;
}

int
ready(int inot_fd, char* file_to_be_watched)
{
    int flag;

    // watch cycle
    int length;
    char buffer[BUF_LEN];
    /* read buffer */
    length = read(inot_fd, buffer, BUF_LEN);
        
    /* process events */
    int i = 0;
    struct inotify_event* event;
    for(i = 0; i < length; i += EVENT_SIZE + event->len)
    {
        flag = 0;
        event = (struct inotify_event*) &buffer[i];

        if (strcmp(basename(file_to_be_watched), event->name) != 0) 
            continue;

        if (event->mask & IN_CREATE)
            flag = 1;

        if (event->mask & IN_MODIFY)
            flag = 1;

        if (event->mask & IN_CLOSE_WRITE)
            flag = 1;

        if (event->mask & IN_DELETE)
            flag = 2;

        if (flag > 0)
            return flag;
    } // while (i < length)

    return flag;
}

int 
watch(char* filename, int sock_fd)
{
    // check if filename exist
    char* file_to_be_watched = filename;

    if (access(file_to_be_watched, F_OK))
    {
        printf("No file %s\n", filename);
        exit(1);
    }

    char* fpath = strdup(file_to_be_watched);
    char* path_to_be_watched = dirname(fpath);
 
    /* Initialize inotify */
    int fd = inotify_init();

    if (fcntl(fd, F_SETFL, O_NONBLOCK) < 0)  // error checking for fcntl
        exit(2);

    /* Add inotify watch */
    int wd = inotify_add_watch(
        fd, path_to_be_watched,
        IN_CLOSE_WRITE | IN_MODIFY | IN_CREATE | IN_DELETE
    );

    if (wd == -1)
        printf("Could not watch : %s\n", path_to_be_watched);
    else
        printf("Watching : %s\n", path_to_be_watched);

    // get filesize 
    sprintf(
        data, "SET Filesize[%s] == %d\n",
        file_to_be_watched, 
        get_filesize(file_to_be_watched)
    );

    // watch
    int nfds = 2;
    int rc;
    int ready_flag;

    struct pollfd fdset[1];
    fdset[0].fd = fd;
    fdset[0].events = POLLIN;
    fdset[1].fd = sock_fd;
    fdset[1].events = POLLIN;

    while(1)
    {
        rc = poll(fdset, nfds, -1);
        if (rc == -1)
        {
            if (errno == EINTR)
                continue;
            perror("poll");
            exit(1);
        }

        if (fdset[1].revents & POLLIN) // check socket requests
        {
            if (process_request(sock_fd) == -1)
            {
                perror("process_request");
                continue;
            }
        }

        if (fdset[0].revents & POLLIN) // check inotify events
        {
            ready_flag = ready(fd, file_to_be_watched);

            if (!ready_flag)
                continue;

            if (ready_flag == 1) // file was modified
            {
                 snprintf(
                    data, BUF_SIZE,
                    "Filesize[%s] == %d\n",
                    file_to_be_watched, 
                    get_filesize(file_to_be_watched)
                 );
            }

            if (ready_flag == 2) // file was deleted
            {
                 snprintf(
                    data, BUF_SIZE,
                    "The file %s was deleted.\n",
                    file_to_be_watched
                 );
            }

        }  // if (fdset[0].revents & POLLIN)


    } // while(1)

    free(fpath);
    inotify_rm_watch(fd, wd);
    close(fd);
}
