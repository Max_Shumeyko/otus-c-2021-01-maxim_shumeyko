#ifndef __DATA_TYPES__
#define __DATA_TYPES__

struct parameters_
{
    char* filename;
    bool daemonize;
    char* domain;
};  

void free_parameters(struct parameters_* parameters);

#endif
