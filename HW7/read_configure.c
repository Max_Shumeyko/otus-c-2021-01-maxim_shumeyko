#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

#include "data_types.h"

void read_configure(struct parameters_* parameters)
{
    // read config file
    FILE* fp = fopen("daemon.conf", "r");
    if (fp == NULL)
    {
        perror("Can't find config file: daemon.conf");
        exit(1);
    }

    int line_size;
    size_t n;
    char* line = malloc(300);
    char word[200];
    
    int param = 0;
    while ((line_size = getline(&line, &n, fp)) != 1)
    {
        sscanf(line, "%s", word);

        if (strcmp(word, "DOMAIN=") == 0)
        {
            line += strlen(word);
            sscanf(line, "%s", word);
            parameters->domain = strdup(word);
            param += 100;
        }
        else if (strcmp(word, "FILENAME=") == 0)
        {
            line += strlen(word);
            sscanf(line, "%s", word);
            parameters->filename = strdup(word);
            param += 10;
        }
        else if (strcmp(word, "DEMONIZE=") == 0)
        {    
            line += strlen(word);
            sscanf(line, "%s", word);
            if (strcmp(word, "true") == 0)
                parameters->daemonize = true;
            else
                parameters->daemonize = false;
            param += 1;
        }
    }

    if (param != 111)
    {
        perror("Wrong parameters set.");
        exit(1);
    }

	fclose(fp);
}
            
            
    



