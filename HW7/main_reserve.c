#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#include "data_types.h"
#include "read_configure.h"
#include "daemonize.h"
#include "trace.h"

struct parameters_ parameters = {NULL, false, NULL};

int
main()
{
    // read the configure file
    printf("Reading the configure from daemon.conf ... ");
    read_configure(&parameters);
    printf("DONE.\n");

    // run server socket
    pid_t pid = fork();
    printf("Running tf_socket ... ");
    if (pid == -1) 
    {
        perror("fork");
        exit(1);
    }
    else if (pid == 0)
    {
        execl("./socket_server", "./socket_server", NULL);
    }
    printf("DONE.\n");

    usleep(300000);
    char* socket_path = realpath("./tf_socket", NULL);

    printf("HINT: \n ---\n"
           "> nc -U tf_socket\n"
           "GET\n --- \n"
    );

    // run inotify watcher
    char* fullpath = realpath(parameters.filename, NULL);

    pid = fork();
    if (pid == -1)
    {
        perror("fork");
        exit(1);
    }
    else if (pid == 0)
    {
        watch(fullpath, socket_path);
    }
    usleep(300000);

    // Daemonize
	char* rpath = realpath(parameters.filename, NULL);
    printf("Watching [%s].\n", rpath);
    if (parameters.daemonize)
    {
        printf("Daemonizing ...\n");
        daemonize("inotify daemon");
    }

	free(rpath);
    free_parameters(&parameters);
    free(socket_path);
    free(fullpath);
    return 0;
}

