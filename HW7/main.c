#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <sys/wait.h>
#include <sys/types.h>

#include "data_types.h"
#include "tf_socket.h"
#include "read_configure.h"
#include "daemonize.h"
#include "trace.h"

struct parameters_ parameters = {NULL, false, NULL};

int
main()
{
    // read the configure file
    read_configure(&parameters);
    printf(
        "HINT: \n ---\n"
        "> nc -U socket\n"
        "GET\n --- \n"
    );

    // Daemonize
    if (parameters.daemonize)
    {
        printf("Daemonizing ...\n");
        daemonize("inotify daemon");
    }

    // run server socket
    int sock;
    start_socket(&sock, "socket");

    // run inotify watcher
    char* fullpath = realpath(parameters.filename, NULL);
    watch(fullpath, sock);

    close(sock);
    free_parameters(&parameters);
    free(fullpath);
    return 0;
}

