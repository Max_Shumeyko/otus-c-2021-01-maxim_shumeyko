#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <sys/un.h>
#include <unistd.h>

int
start_socket(int* sock_id, char* server_name)
{
    int sock;//, msgsock;//, rval;
    struct sockaddr_un server;

    sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sock < 0)
    {
        perror("socket");
        exit(1);
    }

    server.sun_family = AF_UNIX;
    strcpy(server.sun_path, server_name);
    int option = 1;
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));
    if (bind(sock, (struct sockaddr*) &server, sizeof(struct sockaddr_un)))
    {
        perror("bind");
        exit(1);
    }

    if (listen(sock, 5) == -1)
    {
        perror("listen");
        exit(1);
    }

    *sock_id = sock;

    return 0;
}
