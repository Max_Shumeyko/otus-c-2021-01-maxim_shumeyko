#include <stdbool.h>
#include <stdlib.h>

#include "data_types.h"

void free_parameters(struct parameters_* parameters)
{
    free(parameters->filename);
    free(parameters->domain);
}


