#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mlog.h"
#include "side_lib.h"

void f1_with_error()
{
    M_LOG("Call f1_with_error\n");

    void* err_ptr = NULL;
    if (err_ptr == NULL)
    {
        M_ERR("f1 malloc error\n");
        return;
    }

    M_LOG("f1 Done.\n");
}

void f2_hard_task()
{
    M_LOG("Call f2_hard_task\n");
    sleep(1);
    M_LOG("f2 Done.\n");
}

void f3_with_state()
{
    M_LOG("Call f3_with_state\n");
    f1_with_error();
    M_LOG("half of f3 done.\n");
    f2_hard_task();

    // add state
    int arg1 = 1, arg2 = 2;
    const char state[] = "f3 done";
    M_LOG("f3 STATE: arg1: %d, arg2: %d; state: %s\n",
          arg1, arg2, state);

    M_LOG("f3 Done.\n");
}

void f4()
{
    M_LOG("Call f4\n");
    f3_with_state();
    side_lib_func();
    M_LOG("f4 Done.\n");
}

void f5()
{
    M_LOG("Call f5\n");
    f4();
    M_LOG("f5 Done.\n");
}


int main()
{
    printf("Logging library test.\n");
    const char log_fname[] = "log.txt";

    if (m__init_filename("log.txt") != 0)
    {
        printf("Failed initialize the logging. Exit.\n");
        return -1;
    }

    f5();
    m__close();

    printf("Work done. Check %s\n", log_fname);

    return 0;
}
