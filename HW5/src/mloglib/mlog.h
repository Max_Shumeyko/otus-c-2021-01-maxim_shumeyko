#ifndef __M_REP__
#define __M_REP__

#include <stdio.h>

char* prepare_message(char* message);

unsigned long m__get_time_micro();

void
m__add_message(const char* date,
               const char* fname,
               int         line,
               const char* format, 
               ...);

void
m__add_error_message(const char* date,
               const char* fname,
               int         line,
               const char* format, 
               ...);

int m__init_filename(char* filename);
void m__close();


#define M_INIT(filename) {m__init_filename(filename);}

#define M_LOG(format, ...) {\
  m__add_message(__DATE__, __FILE__, __LINE__, format, ##__VA_ARGS__);\
}     

#define M_ERR(format, ...) {\
  m__add_error_message(__DATE__, __FILE__, __LINE__, format, ##__VA_ARGS__);\
}

#define M_CLOSE() {m__close();}

#endif /* __M_REP__ */



