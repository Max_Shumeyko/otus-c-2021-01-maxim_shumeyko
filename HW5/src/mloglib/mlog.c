#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>
#include <execinfo.h>
#include <sys/time.h>

#define FLEN 200
#define BT_BUF_SIZE 100
#define MSG_LEN 2000

static char M_FILENAME[FLEN];
static FILE* MFILE;
static unsigned long t_start = -1;

unsigned long
m__get_time_micro()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);

    if (t_start == -1)
    {
        t_start = 1000000 * tv.tv_sec + tv.tv_usec;
        return 0;
    }
    return 1000000 * tv.tv_sec + tv.tv_usec - t_start;
}

static bool 
mfile_uninitialized(FILE* f)
{
    if (f == NULL)
    {
        printf("Error: LOG file isn't initialized.\n");
        return true;
    }
    return false;
}

int 
m__init_filename(char* filename)
{
    strncpy(M_FILENAME, filename, FLEN);
    MFILE = fopen(M_FILENAME, "a");
    if (mfile_uninitialized(MFILE))
        return 1;
    return 0;
}

void
m__add_message(const char* date,
               const char* fname,
               int         line,
               const char* format, 
               ...)
{
    if (mfile_uninitialized(MFILE))
        return;

    // write metadata
    char log_msg[MSG_LEN];\
    snprintf(log_msg, MSG_LEN,\
             "%s usec:%-12lu %s:%-6d MESSAGE: ",\
            date, m__get_time_micro(), fname, line);\
    fwrite(log_msg, 1, strlen(log_msg), MFILE);

    // write message
    va_list argptr;
    va_start(argptr, format);
    vfprintf(MFILE, format, argptr);

    fflush(MFILE);
}

void
m__add_error_message(const char* date,
               const char* fname,
               int         line,
               const char* format, 
               ...)
{
    if (mfile_uninitialized(MFILE))
        return;

    // write metadata
    char log_msg[MSG_LEN];\
    snprintf(log_msg, MSG_LEN,\
             "%s usec:%-12lu %s:%-6d ERROR: ",\
            date, m__get_time_micro(), fname, line);\
    fwrite(log_msg, 1, strlen(log_msg), MFILE);

    // write error report
    va_list argptr;
    va_start(argptr, format);
    vfprintf(MFILE, format, argptr);

    fflush(MFILE);

    // do function stack backtrace
    int nptrs;
    void* buffer[BT_BUF_SIZE];
    char** lines;
    
    nptrs = backtrace(buffer, BT_BUF_SIZE);
    lines = backtrace_symbols(buffer, nptrs);
    if (lines == NULL)
    {
        printf("backtrace error\n");
        return;
    }
    const char header[] = "/TRACE\n";
    fwrite(header, 1, strlen(header), MFILE);
    int i;
    for (i = 0; i < nptrs; i++)
    {
        fwrite(lines[i], 1, strlen(lines[i]), MFILE);
        fwrite("\n", 1, 1, MFILE);
    }
    fwrite(header, 1, strlen(header), MFILE);

    fflush(MFILE);
    free(lines);

}

void
m__close()
{
    if (mfile_uninitialized(MFILE))
        return;
    fclose(MFILE);
}





