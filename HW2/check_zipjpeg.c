#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <inttypes.h>

#define NROWS 200
#define MCOLS 200

/*
 * Check if script contains addition data concatenated to .jpg file.
 * If finds concatenated archieve - shows a list of filenames
 * if not - show message 'File doesn't contain zip archieve.'
 */
int main(int argc, char *argv[])
{

    // check input arguments
    if (argc != 2)
    {
        printf("Wrong arguments number! "
            "EXAMPLE: ./check_zipjpeg file.jpg\n");

        return 1;
    }

    if (strstr(argv[1], ".jpg") == NULL)
    {
        printf("Wrong input file format!\n");
        return 1;
    }
    
    // open the input file 
    FILE *finput;
    finput = fopen(argv[1], "r");

    if (!finput)
    {
        printf("Can't open the file: %s\n", argv[1]);
        return 1;
    }

    // check if the given file contains addition data
    uint16_t cur=0;
    unsigned char c;
    long long int jpegsize = 0, filesize = 0; 

    while (fread(&c, 1, 1, finput) == 1) {
        
        filesize += 1; // count total size

        // search for jpeg stop mark
        if (cur != 0xFFD9) {
            cur = ((cur % 256) << 8) + c;
            jpegsize += 1; // count jpeg part size
        }

    }

    bool flag_contains_data = (jpegsize < filesize);

    // try to extract file names from zip subfile
    int files_num = 0;
    char filenames[NROWS][MCOLS] = {0};

    if (flag_contains_data) {

        uint32_t signature, compressed_size, uncompressed_size;
        uint16_t flag_compressed, filename_length, extraFieldLength;

        fseek(finput, jpegsize, SEEK_SET); // start from the end of jpeg part

        // read the header, necessary data and jump to the next one
        while (fread(&signature, sizeof(signature), 1, finput) == 1) {

            // check  local file header signature
            if (signature != 67324752) break; 

            // check if data compressed
            fseek(finput, 2 * 2, SEEK_CUR);
            fread(&flag_compressed, sizeof(flag_compressed), 1, finput);

            // read compressed and uncompressed sizes
            fseek(finput, 2 * 2 + 4, SEEK_CUR);
            fread(&compressed_size, sizeof(compressed_size), 1, finput);
            fread(&uncompressed_size, sizeof(uncompressed_size), 1, finput);

            // read filename and add to array
            fread(&filename_length, sizeof(filename_length), 1, finput);

            if (filename_length > MCOLS) {
                printf("Warning! Cut too long filename.");
                filename_length = MCOLS;
            }

            fread(&extraFieldLength, sizeof(extraFieldLength), 1, finput);
            fread(filenames[files_num], 1, filename_length, finput);
            files_num++;
            
            if (files_num == NROWS) {
                printf("More than %d files packed! Stop reading.", NROWS);
                break;
            }

            // jump to the next block
            fseek(finput, extraFieldLength, SEEK_CUR);

            if (!(flag_compressed)) 
                fseek(finput, uncompressed_size, SEEK_CUR);
            else
                fseek(finput, compressed_size, SEEK_CUR);

        } // end while

    } // end if (flag_contains_data)

    //
    fclose(finput);

    printf("File '%s' contains addition data: %s\n", argv[1],
           flag_contains_data ? "true" : "false");

    if (flag_contains_data) {

        printf("Jpegsize: %lld; filesize: %lld\n", jpegsize, filesize);

        if (files_num > 0) {
            printf("Packed files found:\n");

            for (int i = 0; i < files_num; i++)
                printf("\t%s\n", filenames[i]);
        } else {
            printf("File doesn't contain zip archieve.\n");
        }

    }

    return 0;
}
