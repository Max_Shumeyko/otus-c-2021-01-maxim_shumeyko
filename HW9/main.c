#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>

#include <sys/mman.h>
#include <sys/stat.h>

unsigned long 
get_file_size(int fd)
{
	struct stat st;
	if (fstat(fd, &st) == -1)
	{
		perror("fstat");
		return -1;
	}

	return st.st_size;
}

int
main(int argc, char** argv)
{
	// get filename	
	if (argc != 2)
	{
		perror("Wrong arguments number");
		printf("Calculate crc32 sum for filename\n"
			   "USAGE: %s filename\n", argv[0]);
		exit(1);
	}

	char* filename = argv[1];

	// fill buf using shared memory
	unsigned char* buf;
	int fd = open(filename, O_RDONLY);
	if (fd == -1)
	{
		perror("open");
		exit(1);
	}

	unsigned long size = get_file_size(fd);
	if (size == -1)
	{
		perror("get_file_size");
		exit(1);
	}

	buf = (unsigned char*) mmap(
		NULL, size, PROT_READ, MAP_SHARED, fd, 0
	);
	if (buf == MAP_FAILED)
	{
		perror("mmap err");
		exit(1);
	}

	// calculate crc32 sum
	uint_least32_t crc_table[256];
    uint_least32_t crc; 
	int i, j;

    for (i = 0; i < 256; i++)
    {
        crc = i;
        for (j = 0; j < 8; j++)
            crc = crc & 1 ? (crc >> 1) ^ 0xEDB88320UL : crc >> 1;

        crc_table[i] = crc;
    };

    crc = 0xFFFFFFFFUL;

//	unsigned long size_i = size;
//	for (i = 0; i < size; i++)
//		crc = crc_table[(crc ^ buf[i]) & 0xFF] ^ (crc >> 8);
	unsigned char* buf_start = buf;
    while (size--)
        crc = crc_table[(crc ^ *buf++) & 0xFF] ^ (crc >> 8);

    uint_least32_t result = crc ^ 0xFFFFFFFFUL;
	printf("CRC32SUM[%s] = %x", filename, result);

	munmap(buf_start, size);
	return 0;
}

